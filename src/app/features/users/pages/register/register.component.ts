import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../../../../core/services/user.service';
import { Router } from '@angular/router';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  hidePassword = true;

  registrationForm = new FormGroup({
      firstName: new FormControl('', [Validators.required]),
      lastName: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required]),
      confirmPassword: new FormControl('', [Validators.required]),
    },
    {
      validators: this.passwordsMatch
    }
  );

  cols = 2;
  rowHeight = '92%';

  horizontalPosition: MatSnackBarHorizontalPosition = 'right';
  verticalPosition: MatSnackBarVerticalPosition = 'bottom';
  snackBarDurationInSeconds = 6;

  constructor(
    private userService: UserService,
    private router: Router,
    private _snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.setResponsiveElementValues(window.innerWidth <= 1000);
  }
  handleResize(event: UIEvent) {
    const window = event.target as Window;
    this.setResponsiveElementValues(window.innerWidth <= 1000);
  }

  setResponsiveElementValues(smallerScreen: boolean) {
    if (smallerScreen) {
      this.cols = 1;
      this.rowHeight = '70%';
    } else {
      this.cols = 2;
      this.rowHeight = '92%';
    }
  }

  getEmailErrorMessage() {
    if (this.registrationForm.controls['email'].hasError('required')) {
      return 'Email is required';
    }

    return this.registrationForm.controls['email'].hasError('email') ? 'Not a valid email' : '';
  }

  getFirstNameErrorMessage() {
    if (this.registrationForm.controls['firstName'].hasError('required')) {
      return 'First Name is required';
    }
    return '';
  }

  getLastNameErrorMessage() {
    if (this.registrationForm.controls['lastName'].hasError('required')) {
      return 'Last Name is required';
    }
    return '';
  }

  getPasswordErrorMessage() {
    if (this.registrationForm.controls['password'].hasError('required')) {
      return 'Password is required';
    }
    return '';
  }

  getConfirmPasswordErrorMessage() {
    const confirmPasswordInput = this.registrationForm.controls['confirmPassword'];
    if (confirmPasswordInput.hasError('required')) {
      return 'Confirm Password is required';
    }
    if (confirmPasswordInput.hasError('passwordsDoNotMatch')) {
      return 'Password and Confirm Password do not match';
    }
    return '';
  }

  passwordsMatch(registrationForm: AbstractControl) {
    const confirmPasswordInput = registrationForm.get('confirmPassword');
    const passwordInput  = registrationForm.get('password');

    if (confirmPasswordInput?.value !== passwordInput?.value) {
      // note this will overwrite any other existing errors.
      confirmPasswordInput?.setErrors({ 'passwordsDoNotMatch': true });
      return { 'passwordsDoNotMatch': true };
    }
    return null;
  }

  async createAccount() {
    if (this.registrationForm.invalid) {
      return;
    }
    const registrationUser = this.registrationForm.value;
    const newUser = {
      firstName: registrationUser.firstName!,
      lastName: registrationUser.lastName!,
      email: registrationUser.email!,
      password: registrationUser.password!
    }
    this.userService.createUser(newUser).subscribe(
      (registerResponse) => {
        if (registerResponse) {
          this._snackBar.open('Registration successful', '', {
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
            duration: this.snackBarDurationInSeconds * 1000,
            panelClass: 'notification-success'
          });
          this.login(registrationUser.email!, registrationUser.password!);
        } else {
          this._snackBar.open('Registration unsuccessful', '', {
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
            duration: this.snackBarDurationInSeconds * 1000,
            panelClass: 'notification-error'
          });
        }
      }
    );

  }

  private login(email: string, password: string) {
    this.userService.login(email, password).subscribe(
      async (loginResponse) => {
        if (loginResponse) {
          await this.router.navigateByUrl(`/users/${loginResponse.userId}`);
        } else {
          await this.router.navigateByUrl('/users/login');
        }
      });
  }

}
