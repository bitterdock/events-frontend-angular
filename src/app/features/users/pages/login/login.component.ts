import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  hidePassword = true;

  loginForm = new FormGroup({
      email: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required]),
    }
  );

  getEmailErrorMessage() {
    if (this.loginForm.controls['email'].hasError('required')) {
      return 'Email is required';
    }
    return '';
  }

  getPasswordErrorMessage() {
    if (this.loginForm.controls['password'].hasError('required')) {
      return 'Password is required';
    }
    return '';
  }

  // TODO implement
  login() {
    console.log("Hello");
    console.log(this.loginForm.value);
    if (this.loginForm.invalid) {
      return;
    }
    console.log('do logic');
  }

}
