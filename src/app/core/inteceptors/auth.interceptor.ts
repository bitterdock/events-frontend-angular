import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const authToken = localStorage.getItem('events-token');
    if (authToken) {
      const clonedReq = req.clone({ headers: req.headers.set('X-Authorization', authToken) });
      return next.handle(clonedReq);
    } else {
      return next.handle(req);
    }
  }

}
