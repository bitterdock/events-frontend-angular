import { Injectable } from '@angular/core';
import { LoginSuccessResponse, RegisterSuccessResponse, RegisterUserRequest } from '../models/users';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { shareReplay, tap } from 'rxjs';


@Injectable()
export class UserService {
  private readonly BASE_URL = `${environment.apiUrl}/users`;
  private readonly REGISTER_ENDPOINT = '/register';
  private readonly LOGIN_ENDPOINT = '/login';
  private readonly LOGOUT_ENDPOINT = '/logout';

  private readonly LOCAL_STORAGE_USER_ID_KEY = 'events-user-id';
  private readonly LOCAL_STORAGE_TOKEN_KEY = 'events-token';

  constructor(private http: HttpClient) {
  }

  createUser(newUser: RegisterUserRequest) {
    return this.http.post<RegisterSuccessResponse>(`${this.BASE_URL}${this.REGISTER_ENDPOINT}`, newUser);
  }

  login(email: string, password: string) {
    return this.http.post<LoginSuccessResponse>(`${this.BASE_URL}${this.LOGIN_ENDPOINT}`, { email: email, password: password })
      .pipe(
        tap((res) => { this.setAuthInfo(res) } ),
        shareReplay()
      );
  }

  // TODO error handling, don't want to delete info if not 200.
  logout() {
    return this.http.post(`${this.BASE_URL}${this.LOGOUT_ENDPOINT}`, {})
      .pipe(
        tap((res) => { this.removeAuthInfo() }),
        shareReplay()
      )
  }

  // TODO implement
  getUser(userId: number) {

  }

  // TODO implement, also include updatedUser payload as param
  updateUser(userId: number) {

  }

  public isLoggedIn() {
    return localStorage.getItem(this.LOCAL_STORAGE_USER_ID_KEY) && localStorage.getItem(this.LOCAL_STORAGE_TOKEN_KEY);
  }

  isLoggedOut() {
    return !this.isLoggedIn();
  }

  private setAuthInfo(loginResponse: LoginSuccessResponse) {
    localStorage.setItem(this.LOCAL_STORAGE_USER_ID_KEY, String(loginResponse.userId));
    localStorage.setItem(this.LOCAL_STORAGE_TOKEN_KEY, loginResponse.token);
  }

  private removeAuthInfo() {
    localStorage.removeItem(this.LOCAL_STORAGE_USER_ID_KEY);
    localStorage.removeItem(this.LOCAL_STORAGE_TOKEN_KEY);
  }

}
