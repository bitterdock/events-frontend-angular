
export interface RegisterUserRequest {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
}

export interface LoginSuccessResponse {
  userId: number;
  token: string;
}

export interface RegisterSuccessResponse {
  userId: number;
}
